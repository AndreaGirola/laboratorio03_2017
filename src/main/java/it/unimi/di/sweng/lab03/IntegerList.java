package it.unimi.di.sweng.lab03;

public class IntegerList 
{
	private Node head = null;
	
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		result.append('[');
		Node node = head;
		while(node != null)
		{
			result.append(node.getValue());
			if(node.getNext() != null)
				result.append(' ');
			node = node.getNext();
		}
		result.append(']');
		return result.toString();
	}

	public void addLast(int value) 
	{
		Node node = new Node(value);
		if(head == null)
			head = node;
		else
			head.setNext(node);
	}
}
